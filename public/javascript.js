const levels = [
  [
    ["the"],
    ["house", "is"],
    ["play"]
  ],
  [

  ],
  [ 
    ["dog"], 
    ["cat" ]
  ],
  [],
  [ 
    ["jumps"] 
  ]
]

class Wortschat{
  constructor(klasse, level, buchstaben){
    this.klasse = klasse;
    this.level = level;
    this.buchstaben = buchstaben;
  }
}

const abschnitt51 = new Wortschatz(
  5, 1, ["go", "gone", "going", "cat", "dog"]
)

function parse(text, level, sublevel){
  let words = text.split(' ');
  console.log(words);

  const maxLevel = Math.min(level, levels.length)
  for(let levelIndex = 0; levelIndex < maxLevel; levelIndex++ ){
    const currentLevel = levels[levelIndex];

    const maxSublevel = Math.min(currentLevel.length, sublevel)
    for(let sublevelIndex = 0; sublevelIndex < maxSublevel; sublevelIndex++ ){
      const currentSublevel = currentLevel[sublevelIndex];

      words = words.filter( word => !currentSublevel.includes(word) );
    }
  }
  
  console.log(words);

  return words;
}

document.addEventListener("DOMContentLoaded", event => {
  console.log("started");

  document
    .querySelector("button")
    .addEventListener(
      "click", 
      event => {
        const text = document.querySelector("textarea").value;
        const level = document.querySelector("input[id=level]").value;
        const sublevel = document.querySelector("input[id=sublevel]").value;
        parse( text, level, sublevel );
      }, 
      false
    );
});
